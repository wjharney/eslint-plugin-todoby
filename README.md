# eslint-plugin-todoby

Warns when deadlines are approaching or have passed. **Probably doesn't solve your actual problem.** If your code needs to
behave differently at a certain time, that should live in your code. If you need to _change_ your code at a certain
time, you should probably use an actual work-tracking system. If you use this plugin, then `eslint` is no longer a pure
operation - it has an implicit time dependency, making it harder to have reproducible builds.

So then why does this project exist? Because bad ideas can still be fun ideas!

## Installation

You'll first need to install [ESLint](https://eslint.org/):

```sh
npm i eslint --save-dev
```

Next, install `eslint-plugin-todoby`:

```sh
npm install eslint-plugin-todoby --save-dev
```

## Usage

Add `todoby` to the plugins section of your `.eslintrc` configuration file. You can omit the `eslint-plugin-` prefix:

```json
{
  "plugins": ["todoby"]
}
```

Then configure the rules you want to use under the rules section.

```json
{
  "rules": {
    "todoby/upcoming": "error",
    "todoby/overdue": "error"
  }
}
```

## Rules

<!-- begin auto-generated rules list -->

| Name                               |
| :--------------------------------- |
| [overdue](docs/rules/overdue.md)   |
| [upcoming](docs/rules/upcoming.md) |

<!-- end auto-generated rules list -->
