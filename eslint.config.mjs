import pluginJs from "@eslint/js";
import pluginPlugin from "eslint-plugin-eslint-plugin";
import pluginNode from "eslint-plugin-n";
import * as tseslint from "typescript-eslint";

export default tseslint.config(
  { ignores: ["dist"] },
  pluginJs.configs.recommended,
  pluginPlugin.configs["flat/recommended"],
  pluginNode.configs["flat/recommended-script"],
  ...tseslint.configs.recommendedTypeChecked,
  {
    linterOptions: { reportUnusedDisableDirectives: true },
    languageOptions: { parserOptions: { project: true } },
  },
  {
    files: ["**/*.mjs"],
    extends: [
      tseslint.configs.disableTypeChecked,
      ...pluginNode.configs["flat/mixed-esm-and-cjs"],
    ],
  },
  {
    ignores: ["lib"],
    rules: {
      // Outside of lib isn't published, so we can use dev deps
      "n/no-missing-import": "off",
      "n/no-unpublished-import": "off",
    },
  }
);
