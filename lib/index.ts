/**
 * @fileoverview Warns when deadlines are approaching or have passed.
 * @author WJH
 */

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

import requireIndex from "requireindex";

//------------------------------------------------------------------------------
// Plugin Definition
//------------------------------------------------------------------------------

// import all rules in lib/rules
// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
module.exports.rules = requireIndex(__dirname + "/rules");
