/**
 * @fileoverview A TODO item is overdue.
 * @author WJH
 */
import type { Node } from "estree";
import { subDays, differenceInDays } from "date-fns";
import { baseMeta, createRule } from "../shared";

export default createRule(
  {
    ...baseMeta,
    docs: {
      ...baseMeta.docs,
      url: baseMeta.docs.url + "/overdue.md",
      description: "A TODO item is overdue.",
    },
  },
  (date, options, comment, context) => {
    const now = new Date();
    const cutoffDate = subDays(date, options.cutoff);
    const daysPastCutoff = differenceInDays(now, cutoffDate);
    if (daysPastCutoff > 0) {
      return context.report({
        // This works, but I'm not sure why the types aren't aligned
        node: comment as unknown as Node,
        messageId: "overdue",
        data: {
          quantity: String(daysPastCutoff),
          unit: daysPastCutoff === 1 ? "day" : "days",
        },
      });
    }
  }
);
