/**
 * @fileoverview A TODO item is due soon.
 * @author WJH
 */

import type { Node } from "estree";
import { subDays, differenceInDays } from "date-fns";
import { baseMeta, createRule } from "../shared";

export default createRule(
  {
    ...baseMeta,
    docs: {
      ...baseMeta.docs,
      url: baseMeta.docs.url + "/upcoming.md",
      description: "A TODO item will be due soon.",
    },
  },
  (date, options, comment, context) => {
    const now = new Date();
    const startDate = subDays(date, options.start);
    const daysPastStart = differenceInDays(now, startDate);
    const cutoffDate = subDays(date, options.cutoff);
    // `differenceInDays` truncates fractional days, but we'd rather round up
    // +1 accomplishes that, although it's wrong exactly at midnight
    const daysUntilCutoff = 1 + differenceInDays(cutoffDate, now);
    if (daysPastStart > 0 && daysUntilCutoff >= 0) {
      return context.report({
        // This works, but I'm not sure why the types aren't aligned
        node: comment as unknown as Node,
        messageId: "upcoming",
        data: {
          quantity: String(daysUntilCutoff),
          unit: daysUntilCutoff === 1 ? "day" : "days",
        },
      });
    }
  }
);
