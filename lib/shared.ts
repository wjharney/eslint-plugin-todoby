import type { Rule } from "eslint";
import type { Comment, Node } from "estree";
import { parse as parseDate, isValid, formatDate } from "date-fns";

export const baseMeta = {
  type: "problem",
  docs: {
    recommended: true,
    url: "https://gitlab.com/wjharney/eslint-plugin-todoby/-/tree/main/docs/rules",
  },
  schema: [
    {
      type: "object",
      properties: {
        pattern: { type: "string" },
        patternFlags: { type: "string" },
        cutoff: { type: "integer" },
        format: { type: "string" },
        runIfEnv: {
          patternProperties: {
            "(?:)": { type: ["string", "boolean"] },
          },
          type: "object",
          additionalProperties: false,
        },
      },
    },
  ],
  messages: {
    invalid: 'TODO BY comment has invalid date: "{{date}}".',
    missing: "TODO BY comment is missing a date.",
    overdue: "Task is overdue by {{quantity}} {{unit}}.",
    upcoming: "Task is due in {{quantity}} {{unit}}.",
  },
} as const satisfies Rule.RuleMetaData;

export interface Options {
  pattern?: string;
  patternFlags?: string;
  cutoff?: number;
  start?: number;
  format?: string;
  runIfEnv?: Record<string, boolean | string>;
}

const defaultOptions = {
  pattern: "^\\*?\\s*?TODO\\s*?BY",
  patternFlags: "i",
  cutoff: 0,
  start: 7,
  format: "yyyy-MM-dd",
  runIfEnv: { CI: false },
} satisfies Required<Options>;

/** Merge and validate options provided by rule config and global settings. */
function parseOptions(settings: Options, options: Options): Required<Options> {
  /** Provides additional validation and guidance on top of eslint's JSON schema enforcement. */
  function getOption<K extends keyof Options>(
    key: K,
    validate: (arg: NonNullable<Options[K]>) => boolean,
    guidance: string
  ): NonNullable<Options[K]> {
    const fromOptions = options[key] !== undefined;
    const fromSettings = settings[key] !== undefined;
    if (!fromOptions && !fromSettings) return defaultOptions[key];
    let val: NonNullable<Options[K]>;
    let source: string;
    if (fromOptions) {
      val = options[key]!;
      source = "options";
    } else {
      val = settings[key]!;
      source = "settings";
    }
    const message = `Invalid "${key}" option provided (via ${source}): ${JSON.stringify(
      val
    )}. ${guidance}`;
    try {
      if (validate(val)) return val;
      throw new Error(message);
    } catch (cause) {
      // @ts-expect-error invalid arguments because we're supporting node
      // 14, but `cause` was introduced in node 16. We can safely use it,
      // though, as it is just silently ignored.
      // eslint-disable-next-line n/no-unsupported-features/es-syntax
      throw new Error(message, { cause });
    }
  }
  return {
    format: getOption(
      "format",
      // If it doesn't have any valid symbols, formatDate returns the input string.
      (f) => formatDate(new Date(), f) !== f,
      "Format must be comprised of Unicode date field symbols."
    ),
    cutoff: getOption(
      "cutoff",
      (n) => n > -1e8 && n < 1e8 && !Number.isNaN(n),
      "Cutoff offset must be a number between -100 million and 100 million."
    ),
    start: getOption(
      "start",
      (n) => n > -1e8 && n < 1e8 && !Number.isNaN(n),
      "Starting day must be a number between -100 million and 100 million."
    ),
    pattern: getOption(
      "pattern",
      // Will throw if invalid
      (p) => Boolean(new RegExp(p)),
      "Pattern must be a valid regular expression."
    ),
    patternFlags: getOption(
      "patternFlags",
      // Will throw if invalid
      (f) => Boolean(new RegExp("", f)),
      "Flags must be valid regular expression flags or empty string."
    ),
    // No additional validation needed
    runIfEnv: options.runIfEnv ?? settings.runIfEnv ?? defaultOptions.runIfEnv,
  };
}

function getDateFromMatch({
  input,
  index: matchStart,
  0: match,
  1: date,
}: RegExpExecArray) {
  // If we have a capture group, assume it's the date
  if (date) return date;
  // If not, assume that the date immediately follows the match
  const stripped = input.slice(matchStart + match.length).trim();
  return stripped.match(/^(?<=\W*?)\w+?\W+?\w+?\W+?\w+?\b/)?.[0];
}

export function createRule(
  meta: Rule.RuleMetaData,
  validateDate: (
    date: Date,
    options: Required<Options>,
    comment: Comment,
    context: Rule.RuleContext
  ) => void
): Rule.RuleModule {
  return {
    meta,
    create(context) {
      const options = parseOptions(
        // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
        context.settings.todoby ?? {},
        // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
        context.options[0] ?? {}
      );
      const regex = new RegExp(options.pattern, options.patternFlags);
      const now = new Date();

      function check(comment: Comment) {
        const text = comment.value.trim();
        const match = regex.exec(text);
        if (!match) return; // Not a TODO BY comment
        const datestr = getDateFromMatch(match);
        if (!datestr) {
          return context.report({
            // This works, but I'm not sure why the types aren't aligned
            node: comment as unknown as Node,
            messageId: "missing",
          });
        }
        const date = parseDate(datestr, options.format, now);
        if (!isValid(date)) {
          return context.report({
            // This works, but I'm not sure why the types aren't aligned
            node: comment as unknown as Node,
            messageId: "invalid",
            data: { date: datestr },
          });
        }
        validateDate(date, options, comment, context);
      }

      return {
        Program() {
          const { runIfEnv } = options;
          const shouldCheck =
            typeof runIfEnv !== "object" ||
            runIfEnv === null ||
            Object.keys(runIfEnv).every((key) => {
              const target = runIfEnv[key];
              const value = process.env[key];
              const actual =
                typeof target === "boolean" ? Boolean(value) : value;
              return target === actual;
            });

          if (shouldCheck) {
            for (const comment of context.sourceCode.getAllComments()) {
              check(comment);
            }
          }
        },
      };
    },
  };
}
