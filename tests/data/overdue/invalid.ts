import { RuleTester } from "eslint";
import { Options } from "../../../lib/shared";

// The default types use `any`, but we want more precision.
type BaseTestCase = Omit<RuleTester.InvalidTestCase, "options" | "settings"> & {
  options?: Options[];
  settings?: { todoby: Options };
};
type OptionsTestCase = BaseTestCase & Required<Pick<BaseTestCase, "options">>;
type SettingsTestCase = BaseTestCase & Required<Pick<BaseTestCase, "settings">>;

export const basic: BaseTestCase[] = [
  /*
   * Basic tests
   */
  {
    name: "Due yesterday",
    code: "// TODO BY 1999-12-31: Fix Y2K bug",
    errors: ["Task is overdue by 1 day."],
  },
  {
    name: "Invalid ISO-like date",
    code: "// tOdO bY 1111-22-33: Proofread",
    errors: ['TODO BY comment has invalid date: "1111-22-33".'],
  },
  {
    name: "Valid date, wrong format",
    code: "// tOdO bY 12 Feb 2007: Proofread",
    errors: ['TODO BY comment has invalid date: "12 Feb 2007".'],
  },
  {
    name: "No text after TODO BY",
    code: "// TODO BY",
    errors: ["TODO BY comment is missing a date."],
  },
  {
    name: "No date after TODO BY (short text)",
    code: "// TODO BY: Fix",
    errors: ["TODO BY comment is missing a date."],
  },
  {
    name: "Malformed date after TODO BY (long text)",
    code: "// TODO BY sometime, be more decisive",
    errors: ['TODO BY comment has invalid date: "sometime, be more".'],
  },
];

export const withOptions: OptionsTestCase[] = [
  // Pattern
  {
    name: "Due yesterday (custom pattern and flags)",
    code: "/** @todoby 1999-12-31: Prepare for apocalypse **/",
    options: [{ pattern: "@todoby", patternFlags: "" }],
    errors: ["Task is overdue by 1 day."],
  },

  // cutoff
  {
    name: "Positive cutoff moves 'overdue' date earlier",
    code: "// TODO BY 2000-01-01",
    options: [{ cutoff: 1 }],
    errors: ["Task is overdue by 1 day."],
  },

  // Format
  {
    name: "Custom date format with invalid date value",
    code: "// todo BY 33-22-1111: buy a lemon",
    options: [{ format: "dd-MM-yyyy" }],
    errors: ['TODO BY comment has invalid date: "33-22-1111".'],
  },
];

export const withSettings: SettingsTestCase[] = withOptions.map(
  ({ options, ...test }) => ({
    settings: { todoby: options[0] },
    ...test,
  }),
);

export const withOptionsAndSettings: Array<OptionsTestCase & SettingsTestCase> =
  withOptions.map((test) => {
    const opt = test.options[0];
    const todoby: Options = {};
    if (opt.format) todoby.format = "'FORMAT FROM SETTINGS'";
    if (opt.cutoff) todoby.cutoff = -Infinity;
    if (opt.pattern) todoby.pattern = "PATTERN FROM SETTINGS";
    if (opt.patternFlags) todoby.patternFlags = "FLAGS FROM SETTINGS";
    return {
      ...test,
      settings: { todoby },
    };
  });

// Technically not *all*, but do you really care?
export const all = [
  ...basic,
  ...withOptions,
  ...withSettings,
  ...withOptionsAndSettings,
];

export const runIfEnvInOptions: OptionsTestCase[] = all.map((test) => ({
  ...test,
  options: [
    {
      ...test.options?.[0],
      runIfEnv: { NO_TODOBY: "please" },
    },
  ],
}));

export const runIfEnvInSettings: SettingsTestCase[] = all.map((test) => ({
  ...test,
  settings: {
    ...test.settings,
    todoby: {
      ...test.settings?.todoby,
      runIfEnv: { NO_TODOBY: "please" },
    },
  },
}));
