import { RuleTester } from "eslint";
import { Options } from "../../../lib/shared";

// The default types use `any`, but we want more precision.
type BaseTestCase = Omit<RuleTester.ValidTestCase, "options" | "settings"> & {
  options?: Options[];
  settings?: { todoby: Options };
};
type OptionsTestCase = BaseTestCase & Required<Pick<BaseTestCase, "options">>;
type SettingsTestCase = BaseTestCase & Required<Pick<BaseTestCase, "settings">>;

export const basic: BaseTestCase[] = [
  /*
   * Basic tests
   */
  {
    name: "Line comment",
    code: "// TODO BY 9999-11-11: Handle 5-digit years",
  },
  {
    name: "Single-line block comment",
    code: "/* todo by 2020-02-02: Plan palindrome party */",
  },
  {
    name: "Multi-line block comment",
    code: `/* TODO by 2001-03-15:
          - Finish writing TODO list
          */`,
  },
  {
    name: "JSDoc-style comment",
    code: "/** todo BY 2345-06-07: Invent time travel */",
  },
  {
    name: "Due today",
    code: "// ToDo By 2000-01-01: Buy a new calendar",
  },
  {
    name: "Due tomorrow",
    code: "// TODO BY 2000-01-02: Adjust to writing dates with the new year",
  },
  {
    name: "Weird spacing",
    code: "/*    TODO  by   2013-7-23: Trim whitespace        */",
  },
  {
    name: "No spacing",
    code: "//todoBY2024-05-24:fixspacebar",
  },
  {
    name: "No description",
    code: "// TODOBY 2013-06-19",
  },
];

export const withOptions: OptionsTestCase[] = [
  /*
   * Config: custom regex
   */
  {
    name: "Custom pattern with capture group",
    code: "// Before 2000-12-25! Buy a present.",
    options: [{ pattern: "Before ([\\d-]+)!" }],
  },
  {
    name: "Custom pattern without capture group",
    code: "/* Due by 2040-10-29: Eat lunch */",
    options: [{ pattern: "Due by" }],
  },
  {
    name: "Custom pattern flags only (case sensitive)",
    code: "// TODO BY 2038-01-19: Solve 2038 problem",
    options: [{ patternFlags: "" }],
  },
  {
    name: "Custom pattern and flags",
    code: `/**
              * What if JSDoc also had a TODO BY tag?
              * @todoby 2026-11-19: Implement custom tag
              */`,
    // Because of ^ in regex, it only matches using 'm' or 's' flags
    options: [{ pattern: "^.*?@todoby", patternFlags: "m" }],
  },

  /*
   * Config: cutoff
   */
  {
    name: "Negative cutoff moves 'overdue' date later",
    code: "// TODO BY 2000-01-02: Find missing left shoe",
    options: [{ cutoff: -1 }],
  },

  /*
   * Config: format
   */
  {
    name: "Custom date format with valid date value",
    code: "// todoby 09-12-2065: sell a lemon",
    options: [{ format: "dd-MM-yyyy" }],
  },
  {
    name: "Custom date format with ordinal numbers",
    code: "// todoby 2079th?19th?!10th!!! what?!?!",
    options: [{ format: "yo?do?!mo" }],
  },
  {
    name: "Custom date format with uncommon tokens, just for fun",
    code: "/* TODO by Sa during the 23rd week of 02042, in the evening: circumlocute */",
    options: [
      {
        format: "iiiiii 'during the' Io 'week of' RRRRR, B",
        pattern: "TODO by (.+?):",
      },
    ],
  },
];

export const withSettings: SettingsTestCase[] = withOptions.map(
  ({ options, ...test }) => ({
    settings: { todoby: options[0] },
    ...test,
  }),
);

export const withOptionsAndSettings: Array<OptionsTestCase & SettingsTestCase> =
  withOptions.map((test) => {
    const opt = test.options[0];
    const todoby: Options = {};
    if (opt.format) todoby.format = "'FORMAT FROM SETTINGS'";
    if (opt.cutoff) todoby.cutoff = -Infinity;
    if (opt.pattern) todoby.pattern = "PATTERN FROM SETTINGS";
    if (opt.patternFlags) todoby.patternFlags = "FLAGS FROM SETTINGS";
    return {
      ...test,
      settings: { todoby },
    };
  });

export const all = [
  ...basic,
  ...withOptions,
  ...withSettings,
  ...withOptionsAndSettings,
];
