import type { RuleTester } from "eslint";
import type { Options } from "../../../lib/shared";

// The default types use `any`, but we want more precision.
type BaseTestCase = Omit<RuleTester.ValidTestCase, "options" | "settings"> & {
  options?: Options[];
  settings?: { todoby: Options };
};
type OptionsTestCase = BaseTestCase & Required<Pick<BaseTestCase, "options">>;
type SettingsTestCase = BaseTestCase & Required<Pick<BaseTestCase, "settings">>;

export const basic: BaseTestCase[] = [
  /*
   * Basic tests
   */
  {
    name: "Line comment",
    code: `// TODO BY 2000-01-02: something`,
  },
  {
    name: "Single-line block comment",
    code: `/* todo by 2000-01-02: something */`,
  },
  {
    name: "Multi-line block comment",
    code: `/* TODO by 2000-01-02:
            - something
            */`,
  },
  {
    name: "JSDoc-style comment",
    code: `/** todo BY 2000-01-02: something */`,
  },
  {
    name: "Weird spacing",
    code: `/*    TODO  by   2000-01-02: Trim whitespace        */`,
  },
  {
    name: "No spacing",
    code: `//todoBY2000-01-02:something`,
  },
  {
    name: "No description",
    code: `// TODOBY 2000-01-02`,
  },
  {
    name: "Single digit month and day",
    code: `// TODO BY 2000-1-2: something`,
  },
];

export const withOptions: OptionsTestCase[] = [
  /*
   * Config: custom regex
   */
  {
    name: "Custom pattern with capture group",
    code: `// Before 2000-01-02! Something.`,
    options: [{ pattern: "Before ([\\d-]+)!" }],
  },
  {
    name: "Custom pattern without capture group",
    code: `/* Due by 2000-01-02: Something */`,
    options: [{ pattern: "Due by" }],
  },
  {
    name: "Custom pattern flags only (case sensitive)",
    code: `// TODO BY 2000-01-02: something`,
    options: [{ patternFlags: "" }],
  },
  {
    name: "Custom pattern and flags",
    code: `/**
            * What if JSDoc also had a TODO BY tag?
            * @todoby 2000-01-02: something
            */`,
    // Because of ^ in regex, it only matches using 'm' or 's' flags
    options: [{ pattern: "^.*?@todoby", patternFlags: "m" }],
  },

  /*
   * Config: format
   */
  {
    name: "Custom date format with valid date value",
    code: `// todoby 02-01-2000: sell a lemon`,
    options: [{ format: "dd-MM-yyyy" }],
  },
  {
    name: "Custom date format with ordinal numbers",
    code: `// todoby 2000th?2nd?!1st!!! something?!?!`,
    options: [{ format: "yo?do?!mo" }],
  },
  {
    name: "Custom date format with uncommon tokens, just for fun",
    code: `/* TODO by Sa during the 52nd week of 01999, in the evening: something */`,
    options: [
      {
        format: "iiiiii 'during the' Io 'week of' RRRRR, B",
        pattern: "TODO by (.+?):",
      },
    ],
  },
];

export const withSettings: SettingsTestCase[] = withOptions.map(
  ({ options, ...test }) => ({
    settings: { todoby: options[0] },
    ...test,
  }),
);

export const withOptionsAndSettings: Array<RuleTester.ValidTestCase> =
  withOptions.map((test) => {
    const opt = test.options[0];
    const todoby: Options = {};
    if (opt.format) todoby.format = "'FORMAT FROM SETTINGS'";
    if (opt.start) todoby.start = -Infinity;
    if (opt.cutoff) todoby.cutoff = -Infinity;
    if (opt.pattern) todoby.pattern = "PATTERN FROM SETTINGS";
    if (opt.patternFlags) todoby.patternFlags = "FLAGS FROM SETTINGS";
    return {
      ...test,
      settings: { todoby },
    };
  });

export const all = [
  ...basic,
  ...withOptions,
  ...withSettings,
  ...withOptionsAndSettings,
];
