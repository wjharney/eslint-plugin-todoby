/**
 * @fileoverview A TODO item is overdue.
 * @author WJH
 */
import { RuleTester } from "eslint";
import rule from "../../../lib/rules/overdue";
import { withClock, withEnv } from "../../util";
import * as valid from "../../data/overdue/valid";
import * as invalid from "../../data/overdue/invalid";

const Y2K = Date.parse("2000-01-01T12:34:56.789Z");

describe("overdue", () => {
  withClock(Y2K);

  const ruleTester = new RuleTester();
  ruleTester.run("base cases", rule, {
    valid: valid.basic,
    invalid: invalid.basic,
  });
  ruleTester.run("with options", rule, {
    valid: valid.withOptions,
    invalid: invalid.withOptions,
  });
  ruleTester.run("with settings", rule, {
    valid: valid.withSettings,
    invalid: invalid.withSettings,
  });
  ruleTester.run("options take precedence over settings", rule, {
    valid: valid.withOptionsAndSettings,
    invalid: invalid.withOptionsAndSettings,
  });
});

/*
 * Because we only say a comment is invalid when it matches a specific format,
 * malformed comments are always "valid". To guard against mistakes in test
 * cases, we go into the far future, so that every valid case becomes overdue,
 * and therefore will report an error only if correctly formatted.
 */
describe("overdue", () => {
  withClock(864e13); // Maximum possible date value

  const ruleTester = new RuleTester();
  ruleTester.run("in far future, valid tests become invalid", rule, {
    valid: [],
    invalid: valid.all.map((test) => ({
      ...test,
      errors: [{ messageId: "overdue" }],
    })),
  });
});

/*
 * We disable all checks on CI by default, so all invalid comments become valid.
 */
describe("overdue", () => {
  withClock(Y2K);
  withEnv({ CI: "yes" });

  const ruleTester = new RuleTester();
  ruleTester.run("invalid tests are not errors on CI", rule, {
    valid: invalid.all,
    invalid: [],
  });
});

describe("overdue", () => {
  withClock(Y2K);
  withEnv({ NO_TODOBY: "please" });

  const ruleTester = new RuleTester();
  ruleTester.run("rule checked when env in options is matched", rule, {
    valid: [],
    invalid: invalid.runIfEnvInOptions,
  });
  ruleTester.run("rule checked when env in settings is matched", rule, {
    valid: [],
    invalid: invalid.runIfEnvInSettings,
  });
});

/*
 * When the rule is not checked, all invalid comments are valid.
 */
describe("overdue", () => {
  withClock(Y2K);
  withEnv({ NO_TODOBY: "nope" });

  const ruleTester = new RuleTester();
  ruleTester.run("rule not checked when env in options not matched", rule, {
    valid: invalid.runIfEnvInOptions,
    invalid: [],
  });
  ruleTester.run("rule not checked when env in settings not matched", rule, {
    valid: invalid.runIfEnvInSettings,
    invalid: [],
  });
});
