/**
 * @fileoverview A TODO item is due soon.
 * @author WJH
 */
import { RuleTester } from "eslint";
import rule from "../../../lib/rules/upcoming";
import { withClock, withEnv } from "../../util";
import * as cases from "../../data/upcoming/cases";

const Y2K = Date.parse("2000-01-01T12:34:56.789Z");

describe("upcoming", () => {
  withClock(Y2K);

  const ruleTester = new RuleTester();
  ruleTester.run("base cases", rule, {
    valid: [],
    invalid: cases.all.map((test) => ({
      ...test,
      errors: ["Task is due in 1 day."],
    })),
  });
});

describe("upcoming", () => {
  withClock(864e13); // Maximum possible date value

  const ruleTester = new RuleTester();
  ruleTester.run("in far future, cases are valid", rule, {
    valid: cases.all,
    invalid: [],
  });
});

describe("upcoming", () => {
  withClock(-864e13); // Minimum possible date value

  const ruleTester = new RuleTester();
  ruleTester.run("in far past, cases are valid", rule, {
    valid: cases.all,
    invalid: [],
  });
});

/*
 * We disable all checks on CI by default, so all invalid comments become valid.
 */
describe("upcoming", () => {
  withClock(Y2K);
  withEnv({ CI: "yes" });

  const ruleTester = new RuleTester();
  ruleTester.run("invalid tests are not errors on CI", rule, {
    valid: cases.all,
    invalid: [],
  });
});
