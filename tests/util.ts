import FakeTimers from "@sinonjs/fake-timers";

/** Set up tests to run with clock set to the given time */
export const withClock = (now: number): void => {
  let clock: FakeTimers.InstalledClock;
  before(() => {
    clock = FakeTimers.install({ now });
  });
  after(() => {
    clock.uninstall();
  });
};

/** Set up tests to run with the given environment values */
export const withEnv = (testEnv: NodeJS.ProcessEnv): void => {
  let originalEnv: NodeJS.ProcessEnv;
  before(() => {
    originalEnv = process.env;
    // This loses the "cast to string" behavior and the weird prototype, but we
    // don't really need those for these tests
    process.env = { ...originalEnv, ...testEnv };
  });
  after(() => {
    process.env = originalEnv;
  });
};
